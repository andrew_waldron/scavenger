var module = angular.module('starter.menuctrl', []);

module.controller('MenuCtrl', function($scope, $cordovaGeolocation, $location) {
	$scope.JoinAHunt=function(){ 
		$location.url("/tab/join");
	};
	
	$scope.CreateAHunt=function() {
		$location.url("/tab/create");
	};
});
