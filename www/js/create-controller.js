var module = angular.module('starter.createctrl', []);

module.controller('CreateCtrl', function($scope, $cordovaGeolocation, $location) {
	$scope.GoBack= function(){
		$location.url("/menu");
	};

	$scope.user = {};
	$scope.step = 0;
	$scope.allTheHints = [{}];

	$scope.signIn = function(){
		if ($scope.step == 0 && $scope.user.huntName != null){
			$scope.step++;
		}
		 else if($scope.step > 0 && $scope.user.hint!= null)
		{
			 $scope.step++;
		}
	};

	$scope.addHint = function() {
		$scope.allTheHints.push({});
	};

	$scope.save = function(){
		var db = new Firebase("https://fusescavenger.firebaseio.com/");
		authenticateFirebase(db);

		var saveData = getSaveData();

		var dbHunt = db.child('hunts/' + $scope.user.huntName);
		dbHunt.set(saveData, resetTheScreen);
	};

	function getSaveData() {
		var saveData = {
			name: $scope.user.huntName,
			steps: getAllSteps()
		};

		if ($scope.user.password) {
			saveData.password = $scope.user.password;
		}

		return saveData;
	}

	function getAllSteps() {
		var mappedSteps = [];
		$scope.allTheHints.forEach(function(hint) {
			mappedSteps.push({
				answer: hint.answer,
				hint: hint.description
			});
		});

		return mappedSteps;
	}

	function resetTheScreen() {
		$scope.user = {};
		$scope.step = 0;
		$scope.allTheHints = [{}];
		$scope.$apply();
	}

	function authenticateFirebase(db) {
		var AUTH_TOKEN = "awflHFrN6gU5KYtBrmRLqygjMZbTp9w9H2IzfczI";
		db.authWithCustomToken(AUTH_TOKEN, function(error, authData) {
			if (error) console.log("Login Failed!", error);
			else console.log("Login Succeeded!", authData);
		});
	}

});
