var module = angular.module('starter.playctrl', ['starter.services']);

module.controller('PlayCtrl', function($scope, Hunts, $ionicPopup, $location) {
	$scope.GoBack= function(){
		$location.url("/menu");
	};

    $scope.hunts = []
    Hunts.get(function(found) {
        
       $scope.hunts = found;
        $scope.$apply();
    });
    
    $scope.shareStuff = {};
    $scope.selectedHunt = {};
    $scope.title = "Select Hunt";
    $scope.gameState = {

    	isPlayingTheGame: false,
    	hintNumber: 0
    };
    $scope.answer = "";

    $scope.exitHunt = function() {

    	$scope.gameState.isPlayingTheGame = false;
    	$scope.selectedHunt = {};
    }

    $scope.correctAnswer = function(answer) {

    	if (answer === $scope.selectedHunt.steps[$scope.gameState.hintNumber].answer)
    	{
    		if ($scope.selectedHunt.steps.length - 1 === $scope.gameState.hintNumber)
    		{
    			                 var alert = $ionicPopup.show({
				                template: 'Congratulations!',
				                title: 'You\'ve won\!',
				                scope: $scope,
				                buttons: [{
					                text: '<b>Ok.</b>',
					                type: 'button-positive'
				                }]
          				  });
    			                 $scope.exitHunt();
    		}
    		else {
    			   		$scope.gameState.hintNumber++;
    		}

    	}

    	else {
							var alert = $ionicPopup.show({
				                template: 'Incorrect Answer.',
				                title: 'Try Again',
				                scope: $scope,
				                buttons: [{
					                text: '<b>Ok.</b>',
					                type: 'button-positive'
				                }]
          				  });

    	}
    }

    $scope.joinHunt = function(hunt) {

        if (hunt.password === "") {
			$scope.gameState.isPlayingTheGame = true;
			$scope.selectedHunt = hunt;
        } 
        else {
            var myPopup = $ionicPopup.show({
                template: '<input type="password" ng-model="shareStuff.passcode">',
                title: 'Enter Hunt Password',
                scope: $scope,
                buttons: [{
                    text: 'Cancel'
                }, {
                    text: '<b>Enter</b>',
                    type: 'button-positive',
                    onTap: function() {
                        if ($scope.shareStuff.passcode === hunt.password) {
                        	$scope.gameState.isPlayingTheGame = true;
                        	$scope.selectedHunt = hunt;
                            console.log("Password successful");
                            $scope.title = hunt.name + "\'s Hunt";
                        	// window.location="templates/tab-play.html"
                        	$scope.gameState.hintNumber = 0;


                        } else {
                            console.log("Password incorrect");
                            var alert = $ionicPopup.show({
				                template: 'Try again.',
				                title: 'Incorrect Password',
				                scope: $scope,
				                buttons: [{
					                text: '<b>Ok.</b>',
					                type: 'button-positive'
				                }]
          				  });
                        }
                    }
                }]
            });
        }
    };
});